+++
title = "Assigning a hash to another hash"
date = "2014-03-22T10:37:50-04:00"
categories = ["perl", "hash"]
+++

A hash can be assigned to a key of another hash using hash reference.

For a hash %HASH, it's reference is obtained by `\%HASH`.

In the below example, the `%fruit_colors` and `%veg_colors` are assigned to the `%food_colors` hash.

```perl
use Data::Dumper;

my %food_colors = (
                   Fruits     => undef,
                   Vegetables => undef
                  );

my %fruit_colors = (
                    Apple  => "red",
                    Banana => "yellow"
                   );

my %veg_colors = (
                  "Green pepper"  => "green",
                  Potato          => "white"
                 );
print Dumper(\%food_colors);
print Dumper(\%fruit_colors);
print Dumper(\%veg_colors);

$food_colors{Fruits}     = \%fruit_colors;
$food_colors{Vegetables} = \%veg_colors;
print Dumper(\%food_colors);
```
