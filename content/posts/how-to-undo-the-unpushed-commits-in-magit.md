+++
title = "How to undo the unpushed commits in magit?"
date = "2014-10-15T11:17:52-04:00"
categories = ["magit", "git", "emacs"]
+++
In Magit buffer `*magit: ..*`,

* Hit `x`
* Select `origin/master`

Above action will undo all the commits that haven't been pushed to github.com. It will still preserve the modified states of the yet-to-be staged/committed files.
