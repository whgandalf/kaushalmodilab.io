+++
title = "How to quickly create a table in org mode"
date = "2014-05-28T12:44:06-04:00"
categories = ["org", "emacs", "table"]
+++

Set the buffer major mode to `org-mode`. If the file extension is a `.org`, that major mode would be set automatically by emacs.

1. Start writing the headers of the table starting with a vertical pipe `|`.
```
| Header1 | Header2 | Header3 |▮
```

2. Once you have finished writing the headers, do `C-u C-c -` to insert a horizontal line above the header row.
```
|---------+---------+---------|
| Header1 | Header2 | Header3 |▮
```

3. Then do `C-c -` to insert a horizontal line below the header row.
```
|---------+---------+---------|
| Header1 | Header2 | Header3 |▮
|---------+---------+---------|
```

4. Go down one row ( `C-n` ) and hit <kbd>TAB</kbd> and org-mode will figure out that you need to create a new row and will put the cursor in the first cell of the new row.
```
|---------+---------+---------|
| Header1 | Header2 | Header3 |
|---------+---------+---------|
|▮        |         |         |
```

5. You can now use <kbd>TAB</kbd> and <kbd>Shift</kbd> + <kbd>TAB</kbd> to navigate the cells and new rows will be created when you hit <kbd>TAB</kbd> when you are in the last cell of the last created row.

    When you want to close the table with a bottom border, hit `C-c -` when the cursor is in the last row.

    ```
    |---------+---------+---------|
    | Header1 | Header2 | Header3 |
    |---------+---------+---------|
    | A       | B       | C▮      |
    |---------+---------+---------|
    ```
