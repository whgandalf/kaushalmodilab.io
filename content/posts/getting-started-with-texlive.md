+++
title = "Getting started with texlive"
date = "2014-05-29T17:32:05-04:00"
categories = ["texlive", "latex"]
+++

* `man tlmgr` Get help on TeX Live Manager
* `tlmgr info <PACKAGE>` Display detailed information about <PACKAGE>, such as the installation status and description
* `tlmgr update <PACKAGE>` Install <PACKAGE>
* `tlmgr update --list` Just report what needs to be updated
* `tlmgr update --all` Make the local TeX installation correspond to what is in the package respository
