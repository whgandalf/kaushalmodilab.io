+++
title = "Returning a matched group in Matlab"
date = "2014-05-20T15:43:44-04:00"
categories = ["matlab", "regex", "grouping", "cell", "token"]
+++

In Matlab, the groups that are defined in parentheses are saved as _tokens_.

Here is a example where I am retreiving the first matched group or token.

```matlab
string = 'adcout1_TAG_i.txt';
[tok] = regexp(string, '([^_]+).*[iq]\..*', 'tokens');
datapath_point = char(tok{1}); % convert cell to string
% returns 'adcout1' (without quotes)
```

Note that 'tokens' argument to the Matlab regexp function makes that function return a cell of tokens. In the above example that is stored in `tok` cell. In order to use that cell element as a string, I need to first convert it to a string using the `char` function.
