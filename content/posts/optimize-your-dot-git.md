+++
date = "2016-11-28T15:51:11-05:00"
title = "Optimize your .git"
slug = "optimize-your-dot-git"
categories = [
  "git",
]

+++

I was reviewing what was occupying the maximum disk space using the [`ncdu`][ncdu] command line utility. One of the top candidates was the git clone of [`org-mode`][org]. It was using 2.4GB of my disk space!

Surprised by that, I started looking around[^1] if there was a way to optimize the cloned git repositories i.e. the `.git/` directories. And sure enough, there **was** a way.

From [this SO solution][so], all I needed to do was run the below in the git repo directory.

```bash
git reflog expire --all --expire=now
git gc --prune=now --aggressive
```

After running the above, the `org-mode` git repo shrunk down to 68MB!

---

I will find myself needing this for various git projects. So I created an alias called `git_optimize` for my `tcsh` shell.

```tcsh
alias git_optimize 'git reflog expire --all --expire=now; \\
                    git gc --prune=now --aggressive'
```

[ncdu]: https://dev.yorhel.nl/ncdu
[org]: http://orgmode.org/cgit.cgi/org-mode.git/log/
[^1]: I mean, started googling :)
[so]: http://stackoverflow.com/a/2116892/1219634
