+++
title = "Escaping dollar signs in tcsh"
date = "2014-03-06T16:12:56-05:00"
categories = ["tcsh", "find", "escaping", "regex"]
+++

Thanks to [this](http://stackoverflow.com/questions/3571743/csh-alias-with-perl-one-liner-evaluates-when-alias-is-created-and-not-when-alias) StackOverflow post, I found how to escape a $ sign in a regex expression in a tcsh alias. BUT it is UGLY!

I wanted to set an alias for a find command containing -regex. For simplicity I will use this example:
```sh
find . -type f -regex '.*\.txt$'
```
This expression simply gives a list of all *.txt files in any directory under the current path.

When I want to create an alias for that in tcsh, the $ sign has to be escaped as shown below:
```sh
alias findtxt "find . -type f -regex '.*txt'\"\$"''"
```

A simple `$` has to be written as `'\"\$"'`!!!

Granted that I would get the same result if I did `alias findtxt "find . -type f -regex '.*txt'"`. But it was an exercise to figure out how to escape a $ in tcsh.
