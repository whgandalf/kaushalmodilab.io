+++
title = "How to change upper/lower case in emacs regex search-replace?"
date = "2014-03-14T09:46:58-04:00"
categories = ["emacs", "regex", "replace", "upcase", "downcase"]
+++

* Do `M-x query-replace-regexp` or use the default binding `C-M-%` to active the regex search-replace.
* Enter the regex for the strings to be replaced in the "Query regexp:" field; Example: If I want to convert I\_data and Q\_data to i\_data and q\_data respectively, then my search regular expression will be `\([iq]\)_data`.
* It is important to use the **escaped** grouping brackets `\(` `\)` to wrap an expression that you want to upcase or downcase.
* In the "Query replace:" field, the expression will be `\,(downcase \1)_data`[^1].

[^1]: Use `\,(upcase \REGEXGROUPNUMBER)` to convert to upper case.
