+++
title = "Binary <> Decimal <> Hex conversion in Python"
date = "2014-03-14T17:04:04-04:00"
categories = ["conversion", "python"]
+++

* Binary -> Decimal
```python
int('10010100', 2)
```

* Hexadecimal -> Decimal
```python
int('94', 16)
```

* Decimal -> Hexadecimal
```python
hex(148)[2:]
```
The `[2:]` truncates the `0x` prefix added to hex output string.

* Decimal -> Binary
```python
bin(148)[2:]
```
The `[2:]` truncates the `0b` prefix added to binary output string.

Other conversions can be derived from the ones above.

* Hexadecimal -> Binary
```python
bin(int('94', 16))[2:]
```

* Binary -> Hexadecimal
```python
hex(int('10010100', 2))[2:]
```
