+++
categories = [
  "unix",
]
date = "2016-11-23T17:07:26-05:00"
title = "Check If a Command Exists from Shell Script"

+++

I often need to check if a particular executable is present in the `PATH` before I can proceed with what I am doing in a shell script. Also I need to work with both `tcsh` and `bash` scripts. Below presents the different solutions that have worked for these shell scripts for me.

## Bash Shell

The below solution using `hash` was with the help of [this SO solution][so_bash].

```bash
if ! hash some_exec 2>/dev/null
then
    echo "'some_exec' was not found in PATH"
fi
```

Here is the TL;DR from the above SO solution:

> Where bash is your shell/hashbang, consistently use `hash` (for commands) or `type` (to consider built-ins & keywords).
> When writing a POSIX script, use `command -v`.

## Tcsh Shell

Looks like the `tcsh` shell does not have the same `hash` command as the `bash` shell. But the below solution using `where` which I found with the help of [this SO solution][so_tcsh] works fine.

```tcsh
if ( `where some_exec` == "" ) then
    echo "'some_exec' was not found in PATH"
endif
```

[so_bash]: http://stackoverflow.com/a/677212/1219634
[so_tcsh]: http://stackoverflow.com/a/22058620/1219634
