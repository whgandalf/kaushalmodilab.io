+++
categories = ["rust"]
date = "2017-01-12T10:29:28-05:00"
title = "Installing rust toolchain"
slug = "installing-rust-toolchain"

+++

Lately, I have come to know of a command line search utility called [`ripgrep`][3], coded in `rust`, that is capable of faster code searches (at least for my use cases) than `grep` or [`ag`][4]. So I got interested in building the latest versions of `rg` (*ripgrep* binary name) directly from its github *master* branch rather than waiting for its [release binaries][5].

So came the need to install the `rust` toolchain on my machine. Luckily, installing that was super easy; here are the steps:

1. Navigate to https://www.rustup.rs in your web browser.
    - Based on your OS, it will provide you the install instructions. For RHEL 6.6, it asked me to run this `curl` command[^curlshwarn]:

        ```bash
        curl https://sh.rustup.rs -sSf | sh
        ```
2. Once I ran that command, I was faced with the below prompt:

    ![Outcome of running the curl .. | sh command][1]

    Everything looked good, except that I did not want the script to modify the `PATH` variable for me. So I went with the *2) Customize ..* option and disabled that. The main thing to make note of was that all the binaries would be installed in `~/.cargo/bin`.
3. So after running the above, I updated the `PATH` to include `~/.cargo/bin` in my shell config.

That's it! You can then verify the installation by doing a version check.

```bash
rustc --version
```

---

You can learn more about `rustup` from this [`README.md`][2].

[^curlshwarn]: *Normally I wouldn't run a stray `curl .. | sh` command, but I made an exception for this one.*
[1]: /images/installing_rust_toolchain__curl_sh.png
[2]: https://github.com/rust-lang-nursery/rustup.rs/blob/master/README.md
[3]: https://github.com/BurntSushi/ripgrep
[4]: https://github.com/ggreer/the_silver_searcher
[5]: https://github.com/BurntSushi/ripgrep/releases
