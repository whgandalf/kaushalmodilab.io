+++
title = "Search/replace groups using sed"
slug = "search-replace-groups-using-sed"
date = "2014-04-16T14:16:42-04:00"
categories = ["sed", "regex", "escape", "search", "replace"]
+++

This is the most common way of my sed usage:
```sh
echo Good morning | sed 's/\(.*\s\+\).*/\1evening/g'
```

The above script changes `Good morning` to `Good evening`.
Note that the following characters needed the escape character `\`:

* Brackets to create regex groups: `\(` `\)`
* Plus sign to match one or more times: `\+`
* Note that the `.` and `*` characters don't need escaping.
