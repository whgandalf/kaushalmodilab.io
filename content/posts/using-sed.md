+++
title = "Using sed"
date = "2014-03-17T09:31:29-04:00"
categories = ["sed", "unix", "tcsh", "alias"]
+++

*sed* stands for **s**tream **ed**itor.

This is the most common way of my sed usage:
```sh
echo [SOMETHING] | sed 's/old/NEW/g'
```

Based on that, I have this tcsh alias[^1] to get timestamps that I use to append to quick tar backups.
```sh
alias gettimestamp 'date | tr " :" "__" | sed '"'"'s/_[0-9]*_EDT.*//g'"'"''
```

Learn about sed from [here][s1].

[^1]: Note how single quotes are escaped inside single-quoted alias definitions in tcsh.
[s1]: http://www.grymoire.com/Unix/Sed.html
