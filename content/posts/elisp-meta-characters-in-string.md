+++
title = "Elisp Meta Characters in String"
date = "2015-02-07T23:24:13-05:00"
categories = ["elisp", "octal", "meta", "outshine"]
+++

The setup of [outshine][s1] package for emacs requires the user to set the variable `outline-minor-mode-prefix` to `"\M-#"` before the `outline-mode` package (that ships with emacs) is loaded.

The best way to do this is via the Customize interface in emacs.

So I did `M-x customize` and set that variable's value to `"\M-#"` and restarted emacs and that worked. But I was surprised to see that value being saved in the `custom.el` as `'(outline-minor-mode-prefix "\243")`.

After some digging, I came across this [elisp meta-character syntax reference][s2],

> In a string, the 2**7 bit attached to an ASCII character indicates a meta character; thus, the meta characters that can fit in a string have codes in the range from 128 to 255, and are the meta versions of the ordinary ASCII characters.

Thus if the decimal [ASCII value of the `#` character][s3] is 35, the decimal value of `\M-#` will be
"2\*\*7 bit attached to #" i.e. 2\*\*7 + 35 = 128 + 35 = 163. But the value of `\M-#` got stored in `custom.el` as `\243`.

From the [elisp general escape syntax reference][s4],

> An octal escape sequence consists of a backslash followed by up to three octal digits; thus, ‘?\101’ for the character A

.. and doing decimal to octal conversion of 163 gives 243!


[s1]: https://github.com/tj64/outshine
[s2]: https://www.gnu.org/software/emacs/manual/html_node/elisp/Meta_002dChar-Syntax.html#Meta_002dChar-Syntax
[s3]: http://www.theasciicode.com.ar/ascii-printable-characters/number-sign-ascii-code-35.html
[s4]: https://www.gnu.org/software/emacs/manual/html_node/elisp/General-Escape-Syntax.html#General-Escape-Syntax
