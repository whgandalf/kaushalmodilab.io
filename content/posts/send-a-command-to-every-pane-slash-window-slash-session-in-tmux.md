+++
title = "Send a command to every pane/window/session in tmux"
slug = "command-to-every-pane-window-session-in-tmux"
date = "2014-03-06T09:50:21-05:00"
categories = ["tmux", "xargs"]
+++

Here is an excerpt from my .tmux.conf file that allows sending a command to all my "terminals", or to all panes in all windows in all sessions.

```text
# Send the same command to all panes/windows/sessions
bind    E command-prompt -p "Command:" \
          "run \"tmux list-sessions                  -F '##{session_name}'        | xargs -I SESS \
                 tmux list-windows  -t SESS          -F 'SESS:##{window_index}'   | xargs -I SESS_WIN \
                 tmux list-panes    -t SESS_WIN      -F 'SESS_WIN.##{pane_index}' | xargs -I SESS_WIN_PANE \
                 tmux send-keys     -t SESS_WIN_PANE '%1' Enter\""
```

## Usage

- Type the following binding in any tmux pane: `C-z E`[^1]
- `source ~/.alias; clear` (this is entered in the tmux command prompt)

This will source the ~/.alias (analogous to .bashrc or .zshrc) in ALL panes and then clear the terminals as well.

Note that the tmux vars had to be escaped; double hashes '##' were used instead of single hash '#'. The reason is that `tmux run-shell` command will replace the unescaped `#{session_name}`, `#{window_index}` and `#{pane_index}` with their current values before executing the command provided to it.

To avoid that, the hashes have to be escaped by another hash and thus the above code results. When these hashes are escaped, the above variables are evaluated at run time.

[^1]: I have set my tmux prefix to `C-z`
